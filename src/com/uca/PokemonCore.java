package com.uca.core;

import com.uca.dao.PokemonDAO;
import com.uca.dao.TradeDAO;
import com.uca.dao.UserDAO;
import com.uca.entity.PokemonEntity;
import com.uca.entity.TradeEntity;
import com.uca.entity.UserEntity;

import java.util.ArrayList;

public class PokemonCore {

    public static ArrayList<PokemonEntity> getUserPokemons(int userId) {
        return new PokemonDAO().getUserPokemons(userId);
    }

    public static void levelUpPokemon(int userId, int pokemonId) {
        new PokemonDAO().levelUpPokemon(userId, pokemonId);
    }

    public static PokemonEntity getDailyPokemon(int userId) {
        return new PokemonDAO().getDailyPokemon(userId);
    }

    public static void createTrade(int userId, TradeEntity trade) {
        new TradeDAO().createTrade(userId, trade);
    }

    public static ArrayList<TradeEntity> getUserTrades(int userId) {
        return new TradeDAO().getUserTrades(userId);
    }

    public static void acceptTrade(int userId, int tradeId) {
        new TradeDAO().acceptTrade(userId, tradeId);
    }
}
