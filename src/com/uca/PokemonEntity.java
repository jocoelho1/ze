package com.uca.entity;

public class PokemonEntity {
    private int id;
    private String name;
    private int level;

    // Ajoutez ici les autres attributs nécessaires

    public PokemonEntity(int id, String name, int level) {
        this.id = id;
        this.name = name;
        this.level = level;
        // Initialisez ici les autres attributs nécessaires
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    // Ajoutez ici les getters et setters pour les autres attributs

    @Override
    public String toString() {
        return "PokemonEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", level=" + level +
                // Ajoutez ici la représentation des autres attributs
                '}';
    }
}
