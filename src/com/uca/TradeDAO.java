package com.uca.dao;

import com.uca.entity.PokemonEntity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

//package com.uca.entity;
//import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.InputStream;
import java.util.Scanner;
import java.net.URL;
import java.net.URLConnection;



import org.json.JSONArray;

public class PokemonDAO extends _Generic<PokemonEntity> {

    public ArrayList<PokemonEntity> getUserPokemons(int userId) {
        ArrayList<PokemonEntity> entities = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = this.connect.prepareStatement("SELECT * FROM user_pokemon WHERE user_id = ?;");
            preparedStatement.setInt(1, userId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                PokemonEntity entity = new PokemonEntity();
                entity.setId(resultSet.getInt("pokemon_id"));
                entity.setName(resultSet.getString("name"));
                entity.setLevel(resultSet.getInt("level"));

                entities.add(entity);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return entities;
    }

    public void levelUpPokemon(int userId, int pokemonId) {
        try {
            PreparedStatement preparedStatement = this.connect.prepareStatement("UPDATE user_pokemon SET level = level + 1 WHERE user_id = ? AND pokemon_id = ?;");
            preparedStatement.setInt(1, userId);
            preparedStatement.setInt(2, pokemonId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public PokemonEntity getDailyPokemon(int userId) {
        PokemonEntity entity = null;
        try {
            PreparedStatement preparedStatement = this.connect.prepareStatement("SELECT * FROM daily_pokemon WHERE user_id = ?;");
            preparedStatement.setInt(1, userId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                entity = new PokemonEntity();
                entity.setId(resultSet.getInt("pokemon_id"));
                entity.setName(resultSet.getString("name"));
                entity.setLevel(resultSet.getInt("level"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return entity;
    }

    @Override
    public PokemonEntity create(PokemonEntity obj) {
        try {
            String url = "https://pokeapi.co/api/v2/pokemon-species/" + obj.getId();
            URLConnection connection = new URL(url).openConnection();
            InputStream response = connection.getInputStream();
            Scanner scanner = new Scanner(response);
            String responseBody = scanner.useDelimiter("\\A").next();
            scanner.close();

            JSONObject jsonObject = new JSONObject(responseBody);
            String name = jsonObject.getString("name");

            PreparedStatement preparedStatement = this.connect.prepareStatement("INSERT INTO user_pokemon (user_id, pokemon_id, name, level) VALUES (?, ?, ?, ?);", Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(1, obj.getUserId());
            preparedStatement.setInt(2, obj.getId());
            preparedStatement.setString(3, name);
            preparedStatement.setInt(4, obj.getLevel());

            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Creating pokemon failed, no rows affected.");
            }

            try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    obj.setId(generatedKeys.getInt(1));
                    obj.setName(name);
                    return obj;
                } 
                else {
                throw new SQLException("Creating pokemon failed, no ID obtained.");
                }
            }
        } 
        catch (IOException | JSONException | SQLException e) {
            e.printStackTrace();
        }

        return null;
    }


    @Override
    public void delete(PokemonEntity obj) {
        // TODO: Implement the deletion of a Pokémon
    }
}
