package com.uca.entity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class TradeEntity {
    private int id;
    private int userId;
    private int pokemonId;
    private String tradeStatus;
    private String PokemonName;

    // Ajoutez ici les autres attributs nécessaires

    public TradeEntity createTrade(int userId, int pokemonId, String tradeStatus) {
        try {
            String url = "https://pokeapi.co/api/v2/pokemon-species/" + pokemonId;
            URLConnection connection = new URL(url).openConnection();
            InputStream response = connection.getInputStream();
            Scanner scanner = new Scanner(response);
            String responseBody = scanner.useDelimiter("\\A").next();
            scanner.close();

            JSONObject jsonObject = new JSONObject(responseBody);
            String name = jsonObject.getString("name");

            PreparedStatement preparedStatement = tradeDAO.getConnection().prepareStatement("INSERT INTO trades (user_id, pokemon_id, trade_status, pokemon_name) VALUES (?, ?, ?, ?);", Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(1, userId);
            preparedStatement.setInt(2, pokemonId);
            preparedStatement.setString(3, tradeStatus);
            preparedStatement.setString(4, name);

            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Creating trade failed, no rows affected.");
            }

            try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    int tradeId = generatedKeys.getInt(1);
                    return new TradeEntity(tradeId, userId, pokemonId, tradeStatus, name);
                } 
                else {
                    throw new SQLException("Creating trade failed, no ID obtained.");
                }
            }
        } 
        catch (IOException | JSONException | SQLException e) {
            e.printStackTrace();
        }

        return null;
    }


    // Ajoutez ici les getters et setters pour les attributs

    @Override
    public String toString() {
        return "TradeEntity{" +
                "id=" + id +
                ", userId=" + userId +
                ", pokemonId=" + pokemonId +
                ", tradeStatus='" + tradeStatus + '\'' +
                ", name'" + name + '\'' +
                // Ajoutez ici la représentation des autres attributs
                '}';
    }
}
