package com.uca.core;
//package com.uca.dao;
//package com.uca.entity;

import com.uca.dao.UserDAO;
import com.uca.entity.UserEntity;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;


public class UserCore {

    public static ArrayList<UserEntity> getAllUsers() {
        ArrayList<UserEntity> entities = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(getPokemonsFromAPI());
            for (int i = 0; i < jsonArray.length(); i++) {
                UserEntity entity = new UserEntity();
                entity.setId(i + 1);
                entity.setFirstName(jsonArray.getJSONObject(i).getString("name"));
                entity.setLastName("");

                entities.add(entity);
            }
        } 
        catch (JSONException e) {
        e.printStackTrace();
        }

        return entities;
    }

    private static String getPokemonsFromAPI() {
        String url = "https://pokeapi.co/api/v2/pokemon-species?limit=1008";
        try {
            URLConnection connection = new URL(url).openConnection();
            InputStream response = connection.getInputStream();
            Scanner scanner = new Scanner(response);
            String responseBody = scanner.useDelimiter("\\A").next();
            scanner.close();
            return responseBody;
        } 
        catch (IOException e) {
            e.printStackTrace();
        }

        return "";
    }


    public static UserEntity registerUser(UserEntity user) {
        return new UserDAO().create(user);
    }

    public static UserEntity loginUser(String username, String password) {
        return new UserDAO().getUserByUsernameAndPassword(username, password);
    }
}


